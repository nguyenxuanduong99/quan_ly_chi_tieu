package com.uet.moneymanager.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uet.moneymanager.R;
import com.uet.moneymanager.database.DatabaseAccess;
import com.uet.moneymanager.util.DateUtil;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class EditTransaction extends AppCompatActivity implements View.OnClickListener {

    public static final int SELECT_GROUP = 1;

    TextView tvDatePickerValue, tvCancelTransaction, tvSaveTransaction, tvSelectedGroup;
    ImageView ivTransactionGroupIcon;
    EditText etAmountOfMoney, etNote;
    Date selectedDate;
    DatePickerDialog pickerDialog;
    DatabaseAccess databaseAccess;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_transaction);
        findView();
        init();
    }

    private void init() {
        tvDatePickerValue.setOnClickListener(this);
        databaseAccess = new DatabaseAccess(EditTransaction.this);
        etAmountOfMoney.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                etAmountOfMoney.removeTextChangedListener(this);
                try {
                    String givenstring = editable.toString();
                    Long longval;
                    if (givenstring.contains(",")) {
                        givenstring = givenstring.replaceAll(",", "");
                    }
                    longval = Long.parseLong(givenstring);
                    DecimalFormat formatter = new DecimalFormat("#,###,###");
                    String formattedString = formatter.format(longval);
                    etAmountOfMoney.setText(formattedString);
                    etAmountOfMoney.setSelection(etAmountOfMoney.getText().length());
                    // to place the cursor at the end of text
                } catch (Exception nfe) {
                    nfe.printStackTrace();
                }
                etAmountOfMoney.addTextChangedListener(this);
            }
        });
        tvCancelTransaction.setOnClickListener(this);
        tvSaveTransaction.setOnClickListener(this);
        selectedDate = Calendar.getInstance().getTime();
        tvSelectedGroup.setOnClickListener(this);
    }

    private void findView() {
        tvDatePickerValue = findViewById(R.id.tvDatePickerValue);
        etAmountOfMoney = findViewById(R.id.etAmountOfMoney);
        tvCancelTransaction = findViewById(R.id.tvCancelTransaction);
        tvSaveTransaction = findViewById(R.id.tvSaveTransaction);
        tvSelectedGroup = findViewById(R.id.tvSelectedGroup);
        etNote = findViewById(R.id.etTransactionNote);
        tvDatePickerValue.setText(DateUtil.formatDate(Calendar.getInstance().getTime()));
        ivTransactionGroupIcon = findViewById(R.id.ivTransactionGroupIcon);
    }

    @Override
    public void onClick(View view) {
        Intent returnIntent = new Intent();
        switch (view.getId()) {
            case R.id.tvDatePickerValue:
                final Calendar cal = Calendar.getInstance();
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int month = cal.get(Calendar.MONTH);
                final int year = cal.get(Calendar.YEAR);

                pickerDialog = new DatePickerDialog(EditTransaction.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        selectedDate = new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime();
                        tvDatePickerValue.setText(DateUtil.formatDate(selectedDate));
                    }
                },year,month,day);
                pickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                pickerDialog.show();
                break;
            case R.id.tvCancelTransaction:
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
                break;
            case R.id.tvSelectedGroup:
                Intent intent = new Intent(EditTransaction.this, SelectTransactionGroupActivity.class);
                startActivityForResult(intent, SELECT_GROUP);
                break;
        }
    }
}